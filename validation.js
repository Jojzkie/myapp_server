//Validation 
const Joi = require('joi');

//Register Validation
const registerValidation = data => {
    const schema = Joi.object().keys({
        name: Joi.string().min(6).required(),
        email: Joi.string().email().required(),
        password: Joi.string().min(6).required(),
        address: Joi.string().required(),
        age: Joi.number().integer().required(),
    });
    
    return Joi.validate(data, schema);
}


//Login Validation
const loginValidation = data => {
    const schema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().min(6).required(),
    });
    
    return Joi.validate(data, schema);
}

//Post Validation
const postValidation = data => {
    const schema = Joi.object().keys({
        user_name: Joi.string().min(3).required(),
        title: Joi.string().min(6).required(),
        description: Joi.string().min(6).required(),
    });
    
    return Joi.validate(data, schema);
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;
module.exports.postValidation = postValidation;