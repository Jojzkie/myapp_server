const express = require('express');
const router = express.Router();
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const {registerValidation, loginValidation} = require('../validation');

router.get('/', async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch {

    }
});

router.post('/register', async (req, res) => {
    console.log(req.body);
    const { error } = registerValidation(req.body);

    // If there's an error
    if(error) return res.status(400).send(error.details[0].message);

    //Checking if the user is already in the database
    const emailExist = await User.findOne({ email: req.body.email });
    if(emailExist) return res.status(400).send('Email already exist');

    //Hash password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    // Create new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
        address: req.body.address,
        age: req.body.age
    });

    try {
        const savedUser = await user.save();
        // res.send(savedUser)
        res.send('Success!');
    } catch (err) {
        res.status(400).send(err)
    }
});

router.post('/login', async (req, res) => {
    console.log(req.body);
    const { error } = loginValidation(req.body);

    // If there's an error
    if(error) return res.status(400).send(error.details[0].message);

    //Checking if the email exists
    const user = await User.findOne({ email: req.body.email });
    if(!user) return res.status(400).send('Email is not found!');

    //If password is correct
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if(!validPass) return res.status(400).send('Invalid password');

    // res.status(200).json({
    //     success: true,
    //     message: 'Logged In',
    //     updateCause: user,
    // });

    res.send('Success!');
});

module.exports = router;