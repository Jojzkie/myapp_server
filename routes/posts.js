const express = require('express');
const router = express.Router();
const Post = require('../models/Post');
const User = require('../models/User');
const {postValidation} = require('../validation');

router.get('/', async (req, res) => {
    try {
        const posts = await Post.find();
        res.json(posts);
    } catch {

    }
});

router.post('/', async (req, res) => {
    console.log(req.body);
    const { error } = postValidation(req.body);
    let userId = '5d443a5206eca0348cae8f27';

    // // If there's an error
    if(error) return res.status(400).send(error.details[0].message);

    //get User Name
    const user = await User.findOne({ _id: req.body._id });
    if(!user) return res.status(400).send('User not found!');

    //Create new Post
    const post = new Post({
        user_name: user.name,
        title: req.body.title,
        description: req.body.description
    });

    try {
        const savedPost = await post.save();
        // res.send(savedUser)
        res.send('Success!');
    } catch (err) {
        res.status(400).send(err)
    }

    // post.save()
    // .then(data => {
    //     res.send('Success!');
    // })
    // .catch(err => {
    //     res.json({message: err});
    // })
});

module.exports = router;