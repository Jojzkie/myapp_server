const express = require('express');
var cors = require('cors')
const fetch = require("node-fetch");

const app = express();

const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config')
const port = process.env.PORT || 8000;

app.use(cors())

app.use(bodyParser.json());

//Import Routes
const usersRoute = require('./routes/users');
const postsRoute = require('./routes/posts');

app.use('/api/users', usersRoute);
app.use('/api/posts', postsRoute);

//ROUTES  https://www.youtube.com/watch?v=vjf774RKrLc

app.get('/', (req, res) => res.send('Hello World!'))

//Connect To DB

mongoose.connect(process.env.DB_CONNECTION,
                { useNewUrlParser: true, useCreateIndex: true, }, 
                () => console.log('connected to DB!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))